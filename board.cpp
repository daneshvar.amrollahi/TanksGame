#include "board.hpp"
#include <ctime>
#define UP 1
#define LEFT 2
#define UP_AND_LEFT 3

Board::Board(int w, int h, int len, vector < vector<int> > game_map)
{
	width = w;
	height = h;
	unit_length = len;
	setWalls(extractWallsFromGameMap(game_map, len, w, h));
}


vector < pair <Point, Point> > Board::extractWallsFromGameMap(vector < vector<int> > game_map, int len, int width, int height)
{
	int current_x = 0, current_y = 0;
	vector < pair <Point, Point> > walls;

	for (int i = 0 ; i < height ; i++)
	{
		current_x = 0;
		for (int j = 0 ; j < width ; j++)
		{
			int val = game_map[i][j];
			pair <Point, Point> up = make_pair(Point(current_x, current_y), Point(current_x + len, current_y));
			pair <Point, Point> left = make_pair(Point(current_x, current_y), Point(current_x, current_y + len)); 

			if (val == UP)
				walls.push_back(up);
			if (val == LEFT)
				walls.push_back(left);
			if (val == UP_AND_LEFT)
				walls.push_back(up), walls.push_back(left);

			current_x += len;
		}
		current_y += len;
	}

	vector < pair<Point, Point> > marginal_walls = extractMarginalWalls(len, width, height);
	for (int i = 0 ; i < marginal_walls.size() ; i++)
		walls.push_back(marginal_walls[i]);

	return walls;
}

vector < pair<Point, Point> > Board::extractMarginalWalls(int len, int width, int height)
{
	int current_x = width * len - 1;
	int current_y = 0;
	vector < pair<Point, Point> > walls;
	for (int i = 0 ; i < height ; i++)
	{
		pair <Point, Point> right = make_pair( Point(current_x, current_y), Point(current_x, current_y + len) );
		walls.push_back(right);
		current_y += len;
	}

	current_x = 0;
	current_y = height * len - 1;
	for (int i = 0 ; i < width ; i++)
	{
		pair <Point, Point> down = make_pair( Point(current_x, current_y), Point(current_x + len, current_y));
		walls.push_back(down);
		current_x += len;
	}
	return walls;
}


void Board::setWalls(vector < pair<Point, Point> > w)
{
	walls = w;
}

int Board::getWidth()
{
	return width;
}

int Board::getHeight()
{
	return height;
}

int Board::getUnitLength()
{
	return unit_length;
}

vector < pair<Point, Point> > Board::getWalls()
{
	return walls;
} 

void Board::addBullet(Bullet bullet)
{
	bullets.push_back(bullet);
}

vector<Bullet> Board::getBullets()
{
	return bullets;
}

void Board::setBullets(vector <Bullet> bulletss)
{
	bullets = bulletss;
}

int dist(Point p1, Point p2)
{
	return ((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

bool isCellEmpty(Point p, vector <Point> occupied, int len)
{
	for (int i = 0 ; i < occupied.size() ; i++)
		if (dist(p, occupied[i]) < len * len)
			return 0;
	return 1;
}

Point Board::getRandomEmptyCell(vector <Point> occupied)
{
	vector <Point> all;
	for (int i = 0 ; i < width ; i++)
		for (int j = 0 ; j < height ; j++)
		{	
			int current_x = i * unit_length + (unit_length / 2);
			int current_y = j * unit_length + (unit_length / 2);
			if (isCellEmpty(Point(current_x, current_y), occupied, unit_length))
				all.push_back(Point(current_x, current_y));
		}
	int sz = all.size();
	if (sz == 0)
		Point(-1, -1);
	return all[rand() % sz];
}


void Board::addBonus(vector <Point> occupied)
{
	srand(time(NULL));
	if (bonus.size() == 3)
		return;
	if ((rand() % 10) != 0)
		return;

	Point empty_cell = this->getRandomEmptyCell(occupied);

	vector < pair<int, pair<int, int> > > new_bonus = bonus;
	new_bonus.push_back(make_pair(0, make_pair(empty_cell.x, empty_cell.y)));
	this->setBonus(new_bonus);
}

void Board::setBonus(vector < pair<int, pair<int, int> > > bonuss)
{
	bonus = bonuss;
}

vector < pair<int, pair<int, int> > > Board::getBonus()
{
	return bonus;
}