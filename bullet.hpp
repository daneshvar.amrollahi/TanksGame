#ifndef __BULLET_HPP__
#define __BULLET_HPP__

#include <vector>
#include <utility>
#include "src/rsdl.hpp"

using namespace std;
class Bullet
{
private:
	int dx;
	int dy;
	int posx;
	int posy;
	int life;
	int shooter;
public:
	Bullet(int x, int y, int dirx, int diry, int shoorter_tank);
	int getX();
	int getY();

	void move(vector < pair<Point, Point> > walls);
	int getLife();
	int getShooter();
};

#endif
