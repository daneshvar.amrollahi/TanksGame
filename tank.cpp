#include <iostream>
#include "tank.hpp"
#include "board.hpp"
#include <math.h>
using namespace std;

#define PI 3.14159265359
#define NOTHING make_pair(-1, -1)
#define NO_POWER make_pair(-1, make_pair(-1, -1))
#define TIME_UNIT 20

void Tank::setX(int x)
{
	posx = x;
}

void Tank::setY(int y)
{
	posy = y;
}

void Tank::setAngle(double ang)
{
	angle = ang;
}

void Tank::setDx(int dirx)
{
	dx = dirx;
}

void Tank::setDy(int diry)
{
	dy = diry;
}

int Tank::getX()
{
	return posx;
}

int Tank::getY()
{
	return posy;
}

double Tank::getAngle()
{
	return angle;
}

int Tank::getDx()
{
	return dx;
}

int Tank::getDy()
{
	return dy;
}


Tank::Tank(int x, int y, int unit_length)
{
	setX(x * unit_length + (unit_length / 3));
	setY(y * unit_length + (unit_length / 3));
	setAngle(0.0);
	setDx(10);
	setDy(0);
	bullets_count = 0;
	current_power = NOTHING;
}

void Tank::rotateCounterClockwise()
{
	double new_angle = angle - 10;
	setAngle(new_angle);
	new_angle *= (PI / 180.0);
	setDx(cos(new_angle) * 10);
	setDy(sin(new_angle) * 10);
}

void Tank::rotateClockwise()
{
	double new_angle = angle + 10;
	setAngle(new_angle);
	new_angle *= (PI / 180.0); 
	setDx(cos(new_angle) * 10);
	setDy(sin(new_angle) * 10);

}



bool collideWithWall(int x, int y, pair<Point, Point> wall, int unit_length)
{
	x += (unit_length / 6);
	y += (unit_length / 6);

	Point p1 = wall.first, p2 = wall.second;
	if (p1.x == p2.x)
	{
		if (abs(p1.x - x) <= (unit_length / 6))
			if (p1.y <= (y - unit_length / 6) && p2.y >= (y + unit_length / 6))
				return 1;
	}
	if (p1.y == p2.y)
	{
		if (abs(p1.y - y) <= (unit_length / 6))
			if (p1.x <= (x - unit_length / 6) && p2.x >= (x + unit_length / 6))
				return 1;
	}
	return 0;
}

bool collideWithWalls(int x, int y, vector< pair<Point, Point> > walls, int unit_length)
{
	for (int i = 0 ; i < walls.size(); i++)
	{
		pair<Point, Point> wall = walls[i];
		if (collideWithWall(x, y, wall, unit_length))
			return 1;
	}
	return 0;
}

pair<int, pair<int, int> > Tank::collideWithNewPower(Board* board)
{
	vector < pair<int, pair<int, int> > > bonus = board->getBonus();
	int unit_length = board->getUnitLength();
	int nposx = posx + unit_length / 6, nposy = posy + unit_length / 6;
	for (int i = 0 ; i < bonus.size() ; i++)
	{
		pair<int, pair<int, int> > current_power = bonus[i];
		int x = current_power.second.first, y = current_power.second.second;

		if ( (x - nposx) * (x - nposx) + (y - nposy) * (y - nposy) < (unit_length / 3 - 1) * (unit_length / 3 - 1) )
		{
			bonus.erase(bonus.begin() + i);
			board->setBonus(bonus);
			return current_power;
		}
	}
	return NO_POWER;
}


void Tank::addPower(pair<int, pair<int, int> > pow)
{
	int type = pow.first;
	if (type == SHIELD)
	{
		int life = TIME_UNIT * 6;
		powers.push(make_pair(type, life));
	}
}


void Tank::moveForward(Board* board)
{
	vector< pair<Point, Point> > walls = board->getWalls();
	int unit_length = board->getUnitLength();
	if (collideWithWalls(posx + dx, posy + dy, walls, unit_length))
		return;
	setX(posx + dx);
	setY(posy + dy);

	pair<int, pair<int, int> > pow = this->collideWithNewPower(board);
	if (pow == NO_POWER)
		return;

	this->addPower(pow);
}

void Tank::moveBackward(Board* board)
{
	vector< pair<Point, Point> > walls = board->getWalls();
	int unit_length = board->getUnitLength();
	if (collideWithWalls(posx - dx, posy - dy, walls, unit_length))
		return;
	setX(posx - dx);
	setY(posy - dy);

	pair<int, pair<int, int> > pow = this->collideWithNewPower(board);
	if (pow == NO_POWER)
		return;

	this->addPower(pow);

}


Point Tank::getBulletPosition(Board* board)
{
	int unit_length = board->getUnitLength() / 3;
	int center_x = posx + (unit_length / 2), center_y = posy + (unit_length / 2);

	int x = posx + unit_length, y = posy + unit_length / 2;

	double ang_radian = (angle * PI) / 180;


	int ans_x = (x - center_x) * cos(ang_radian) - (y - center_y) * sin(ang_radian) + center_x;
	int ans_y = (x - center_x) * sin(ang_radian) + (y - center_y) * cos(ang_radian) + center_y;
	return Point(ans_x, ans_y);
}

void Tank::shootBullet(Board* board, int shooter)
{
	if (bullets_count == 5)
		return;
	int unit_length = board->getUnitLength();
	unit_length /= 3;
	Point newBullet = getBulletPosition(board);
	Bullet bullet(newBullet.x, newBullet.y, dx, dy, shooter);
	board->addBullet(bullet);
	bullets_count++;
}

void Tank::setBulletsCount(int cnt)
{
	bullets_count = cnt;
}

void Tank::updatePowers()
{	
	if (current_power.second == 0 || current_power.second == -1)
	{
		if (powers.size())
		{
			current_power = powers.front();
			powers.pop();
		}
		else
			current_power = NOTHING;
	}
	else
	{
		current_power.second--;
	}
}

pair<int, int> Tank::getCurrentPower()
{
	return current_power;
}