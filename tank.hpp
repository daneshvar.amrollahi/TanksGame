#ifndef __TANK_HPP__
#define __TANK_HPP__

#include "board.hpp"
#include <vector>
#include <string>
#include <queue>

using namespace std;
class Tank
{
private:
	int posx;
	int posy;
	double angle;
	int dx;
	int dy;
	int bullets_count;

	pair<int, int> current_power;
	queue < pair<int, int> > powers;
public:
	Tank(int x, int y, int unit_length);
	int getX();
	int getY();
	double getAngle();
	int getDx();
	int getDy();

	void setX(int x);
	void setY(int y);
	void setAngle(double ang);
	void setDx(int dirx);
	void setDy(int diry);

	void rotateClockwise();
	void rotateCounterClockwise();

	void moveForward(Board* board);
	void moveBackward(Board* board);



	Point getBulletPosition(Board* board);

	void shootBullet(Board* board, int shooter);

	void setBulletsCount(int cnt);

	pair<int, pair<int, int> > collideWithNewPower(Board* board);

	void addPower(pair<int, pair<int, int> > pow);

	void updatePowers();

	pair<int, int> getCurrentPower();
};

#endif