#ifndef __BOARD_HPP__
#define __BOARD_HPP__

#include <utility>
#include <vector>
#include "src/rsdl.hpp"
#include "bullet.hpp"

#define SHIELD 0
#define SHOTGUN 1
#define DOUBLE_BARREL 2

using namespace std;
class Board
{
private:
	int width;
	int height;
	int unit_length;
	vector < pair<Point, Point> > walls;

	vector <Bullet> bullets;

	vector < pair<int, pair<int, int> > > bonus;

public:
	Board(int w, int h, int len, vector < vector<int> > game_map);
	void setWalls(vector < pair<Point, Point> > w);
	vector < pair<Point, Point> > extractWallsFromGameMap(vector < vector<int> > game_map, int len, int width, int height);
	vector < pair<Point, Point> > extractMarginalWalls(int len, int width, int height);

	int getWidth();
	int getHeight();
	int getUnitLength();

	vector < pair<Point, Point> > getWalls();	
	vector < pair<int, pair<int, int> > > getBonus();
	void addBullet(Bullet bullet);

	vector <Bullet> getBullets();

	void setBullets(vector <Bullet> bulletss);

	Point getRandomEmptyCell(vector <Point> occupied);

	void addBonus(vector <Point> occupied);

	void setBonus(vector < pair<int, pair<int, int> > > bonuss);

};

#endif
