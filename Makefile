a.out: main.o rsdl.o board.o tank.o bullet.o 
	g++ -std=c++11 main.o rsdl.o board.o tank.o bullet.o -l SDL2 -l SDL2_image -l SDL2_ttf -l SDL2_mixer

rsdl.o: src/rsdl.cpp
	g++ -std=c++11 -c src/rsdl.cpp -l SDL2 -l SDL2_image -l SDL2_ttf -l SDL2_mixer 

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

tank.o: tank.cpp
	g++ -std=c++11 -c tank.cpp

bullet.o: bullet.cpp
	g++ -std=c++11 -c bullet.cpp

board.o: board.cpp
	g++ -std=c++11 -c board.cpp
