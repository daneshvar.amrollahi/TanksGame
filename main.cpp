#include <iostream>
#include "src/rsdl.hpp"
#include "board.hpp"
#include "tank.hpp"

using namespace std;

#define TANK1_ID 1
#define TANK2_ID 2
#define UNIT_LENGTH 150
#define ZERO_CHAR '0'
#define WINDOW_NAME "A5"
#define NOTHING make_pair(-1, -1)
#define NORMAL_TANK1_LOCATION "Assets/Tank1-Normal.png"
#define SHIELD_TANK1_LOCATION "Assets/Tank1-Shield.png"
#define NORMAL_TANK2_LOCATION "Assets/Tank2-Normal.png"
#define SHIELD_TANK2_LOCATION "Assets/Tank2-Shield.png"
#define SHIELD_PICKUP_LOCATION "Assets/Shield-Pickup.png"
#define TANK1_MOVE_FORWARD 'w'
#define TANK1_MOVE_BACKWARD 's'
#define TANK1_ROTATE_CLOCKWISE 'd'
#define TANK1_ROTATE_COUNTERCLOCKWISE 'a'
#define TANK1_SHOOT_BULLET 'q'
#define TANK2_MOVE_FORWARD 'i'
#define TANK2_MOVE_BACKWARD 'k'
#define TANK2_ROTATE_CLOCKWISE 'l'
#define TANK2_ROTATE_COUNTERCLOCKWISE 'j'
#define TANK2_SHOOT_BULLET 'u'
#define QUIT 'c'
#define FONT_LOCATION "Assets/TankTrouble.ttf"
#define FONT_SIZE 20
#define BOMB_SOUND_LOCATION "Assets/Sounds/Bomb.wav"

Board readBoard(int _UNIT_LENGTH)
{
	int n,m;
	cin >> n >> m;
	vector < vector<int> > game_map;
	for (int i = 0 ; i < n ; i++)
	{
		vector <int> row;
		for (int j = 0 ; j < m ; j++)
		{
			char c;
			cin >> c;
			row.push_back(c - ZERO_CHAR);
		}
		game_map.push_back(row);
	}

	return Board(m, n, _UNIT_LENGTH, game_map);
}


void printWalls(Window* win, Board* game_board)
{
	vector < pair<Point, Point> > w = game_board->getWalls();
	for (int i = 0 ; i < w.size() ; i++)
	{
		pair<Point, Point> line = w[i];
		win->draw_line(w[i].first, w[i].second, WHITE);
	}

}

void printTank1(Window *win, Tank* tank1, Board* game_board)
{
	int unit_length = game_board->getUnitLength();
	int x = tank1->getX();
	int y = tank1->getY();

	unit_length /= 3;
	double tank_angle = tank1->getAngle();

	pair<int, int> current_power = tank1->getCurrentPower();

	if (current_power == NOTHING)
		win->draw_img(NORMAL_TANK1_LOCATION, Rectangle(Point(x, y), 
			Point(x + unit_length, y + unit_length)), NULL_RECT, (double)tank_angle);

	if (current_power.first == SHIELD)
		win->draw_img(SHIELD_TANK1_LOCATION, Rectangle(Point(x, y), 
			Point(x + unit_length, y + unit_length)), NULL_RECT, (double)tank_angle);	
}

void printTank2(Window *win, Tank* tank2, Board* game_board)
{

	int unit_length = game_board->getUnitLength();
	int x = tank2->getX();
	int y = tank2->getY();
	unit_length /= 3;
	double tank_angle = tank2->getAngle();
	pair<int, int> current_power = tank2->getCurrentPower();

	if (current_power == NOTHING)
		win->draw_img(NORMAL_TANK2_LOCATION, Rectangle(Point(x, y), 
			Point(x + unit_length, y + unit_length)), NULL_RECT, (double)tank_angle);

	if (current_power.first == SHIELD)
		win->draw_img(SHIELD_TANK2_LOCATION, Rectangle(Point(x, y), 
			Point(x + unit_length, y + unit_length)), NULL_RECT, (double)tank_angle);
}

void printTanks(Window* win, Tank* tank1, Tank* tank2, Board* game_board)
{
	printTank1(win, tank1, game_board);
	printTank2(win, tank2, game_board);
}

#define BULLET_RADIUS_COEFFICIENT 30

void printBullets(Window* win, Board* game_board)
{
	vector <Bullet> bullets = game_board->getBullets(); 
	int unit_length = game_board->getUnitLength();
	for (int i = 0 ; i < bullets.size() ; i++)
	{
		Bullet bullet = bullets[i];
		win->fill_circle(Point(bullet.getX(), bullet.getY()), unit_length / BULLET_RADIUS_COEFFICIENT, BLUE);
	}
}

void printBonus(Window* win, Board* game_board)
{
	vector < pair<int, pair<int, int> > > bonus = game_board->getBonus();
	int unit_length = game_board->getUnitLength();
	for (int i = 0 ; i < bonus.size() ; i++)
	{
		int type = bonus[i].first;
		int x = bonus[i].second.first, y = bonus[i].second.second;
		if (type == SHIELD)
			win->draw_img(SHIELD_PICKUP_LOCATION, Rectangle(Point(x - unit_length / 6, y - unit_length / 6),
				Point(x + unit_length / 6, y + unit_length / 6)), NULL_RECT, 0, 0, 0);
	}
}

void printBoard(Window* win, Board* game_board, Tank* tank1, Tank* tank2)
{
	win->clear();
	printWalls(win, game_board);
	printTanks(win, tank1, tank2, game_board);
	printBullets(win, game_board);
	printBonus(win, game_board);
	win->update_screen();
}

pair<Tank, Tank> readTanks(int unit_length)
{
	int x,y;
	cin >> x >> y;
	Tank tank1 = Tank(y, x, unit_length);
	cin >> x >> y;
	Tank tank2 = Tank(y, x, unit_length);
	return make_pair(tank1, tank2);
}

void executeKeyPressTank1(Event event, Tank* tank1, char pressed_char, Board* board)
{
	if (pressed_char == TANK1_MOVE_FORWARD)
		tank1->moveForward(board);
	
	if (pressed_char == TANK1_MOVE_BACKWARD)
		tank1->moveBackward(board);
	
	if (pressed_char == TANK1_ROTATE_COUNTERCLOCKWISE)
		tank1->rotateCounterClockwise();
	
	if (pressed_char == TANK1_ROTATE_CLOCKWISE)
		tank1->rotateClockwise();
	
	if (pressed_char == TANK1_SHOOT_BULLET)
		tank1->shootBullet(board, TANK1_ID);
}

void executeKeyPressTank2(Event event, Tank* tank2, char pressed_char, Board* board)
{
	if (pressed_char == TANK2_MOVE_FORWARD)
		tank2->moveForward(board);
	
	if (pressed_char == TANK2_MOVE_BACKWARD)
		tank2->moveBackward(board);

	if (pressed_char == TANK2_ROTATE_CLOCKWISE)
		tank2->rotateClockwise();

	if (pressed_char == TANK2_ROTATE_COUNTERCLOCKWISE)
		tank2->rotateCounterClockwise();
	
	if (pressed_char == TANK2_SHOOT_BULLET)
		tank2->shootBullet(board, TANK2_ID);
}

bool executeKeyPress(Event event, Tank* tank1, Tank* tank2, Board* board)
{
	char pressed_char = event.get_pressed_key();

	if (pressed_char == QUIT)
		return 0;

	if (pressed_char == TANK1_MOVE_FORWARD || pressed_char == TANK1_MOVE_BACKWARD 
			|| pressed_char == TANK1_ROTATE_CLOCKWISE || pressed_char == TANK1_ROTATE_COUNTERCLOCKWISE 
			|| pressed_char == TANK1_SHOOT_BULLET)
		executeKeyPressTank1(event, tank1, pressed_char, board);

	if (pressed_char == TANK2_MOVE_FORWARD || pressed_char == TANK2_MOVE_BACKWARD 
			|| pressed_char == TANK2_ROTATE_CLOCKWISE || pressed_char == TANK2_ROTATE_COUNTERCLOCKWISE 
			|| pressed_char == TANK2_SHOOT_BULLET)
		executeKeyPressTank2(event, tank2, pressed_char, board);

	return 1;

}

bool executeEvent(Event event, Tank* tank1, Tank* tank2, Board* board)
{
	if(event.get_type() == Event::KEY_PRESS)
	{
		if(executeKeyPress(event, tank1, tank2, board))
			return 1;
		else
			return 0;
	}
	return 1;
}

void moveBullets(Board* board, Tank* tank1, Tank* tank2)
{
	vector <Bullet> bullets = board->getBullets();
	vector < pair<Point, Point> > walls = board->getWalls();
	for (int i = 0 ; i < bullets.size() ; i++)
		bullets[i].move(walls);
	
	vector <Bullet> updated_bullets;
	int tank1_bullets = 0, tank2_bullets = 0;
	for (int i = 0 ; i < bullets.size() ; i++)
		if (bullets[i].getLife() > 0)
		{
			updated_bullets.push_back(bullets[i]);
			tank1_bullets += bullets[i].getShooter() == TANK1_ID;
			tank2_bullets += bullets[i].getShooter() == TANK2_ID;
		}

	board->setBullets(updated_bullets);
	tank1->setBulletsCount(tank1_bullets);
	tank2->setBulletsCount(tank2_bullets);
}

int distance(Point p, Point q)
{
	return (p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y);
}

bool collisionTankBullet(Tank* tank, Bullet bullet, int unit_length)
{
	int x = tank->getX() + (unit_length / 6);
	int y = tank->getY() + (unit_length / 6);

	int radius = UNIT_LENGTH / 6;

	return distance(Point(x, y), Point(bullet.getX(), bullet.getY())) < (unit_length / 6) * (unit_length / 6);
}


int gameOver(Board* board, Tank* tank1, Tank* tank2)
{
	vector <Bullet> bullets = board->getBullets();
	int unit_length = board->getUnitLength();

	for (int i = 0 ; i < bullets.size() ; i++)
	{
		Bullet current_bullet = bullets[i];
		if (collisionTankBullet(tank1, current_bullet, unit_length))
		{
			if (tank1->getCurrentPower().first != SHIELD)
				return TANK1_ID;
			else
			{
				bullets.erase(bullets.begin() + i);
				board->setBullets(bullets);
				return 0;
			}
		}
		if (collisionTankBullet(tank2, current_bullet, unit_length))
		{
			if (tank2->getCurrentPower().first != SHIELD)
				return TANK2_ID;
			else
			{
				bullets.erase(bullets.begin() + i);
				board->setBullets(bullets);
				return 0;
			}
		}
	}
	return 0;
}

vector <Point> getOccupiedCells(Tank* tank1, Tank* tank2, Board* board)
{
	vector <Point> ans;
	ans.push_back(Point(tank1->getX(), tank1->getY()));
	ans.push_back(Point(tank2->getX(), tank2->getY()));
	vector < pair<int, pair<int, int> > > bonus = board->getBonus();
	for (int i = 0 ; i < bonus.size() ; i++)
	{
		Point current_bonus_pos = Point(bonus[i].second.first, bonus[i].second.second);
		ans.push_back(current_bonus_pos);
	}
	return ans;
}

void playEndingSound(Window* win)
{
	win->play_music(BOMB_SOUND_LOCATION);
	win->update_screen();
	delay(1700);
	win->stop_music();
	win->update_screen();	
}

void printGameOverMessage(Window* win, int loser)
{
	playEndingSound(win);
	win->clear();
	string out = "The winner is Tank ";
	if (loser == 1)
		out += "2";
	else
		out += "1";
	win->show_text(out, Point(10, 10), loser == 1 ? RED : GREEN, FONT_LOCATION, FONT_SIZE);
	win->update_screen();
	while (1)
	{
		Event event = win->poll_for_event();
		if(event.get_type() == Event::KEY_PRESS)
			return;
	}
}

void runGame()
{
	Board game_board = readBoard(UNIT_LENGTH);
	pair<Tank, Tank> tanks = readTanks(game_board.getUnitLength());
	Tank tank1 = tanks.first;
	Tank tank2 = tanks.second;

	int width = game_board.getWidth();
	int height = game_board.getHeight();
	int unit_length = game_board.getUnitLength();

	Window win(width * unit_length, height * unit_length, WINDOW_NAME);

	int game_over = 0;
	while (!game_over)
	{
		Event event = win.poll_for_event();
		if(!executeEvent(event, &tank1, &tank2, &game_board))
			break;

		delay(50);
		moveBullets(&game_board, &tank1, &tank2);
		printBoard(&win, &game_board, &tank1, &tank2);


		tank1.updatePowers();
		tank2.updatePowers();

		game_board.addBonus(getOccupiedCells(&tank1, &tank2, &game_board));
		game_over = gameOver(&game_board, &tank1, &tank2);
	}

	printGameOverMessage(&win, game_over);
}

int main()
{
	runGame();
	return 0;
}
