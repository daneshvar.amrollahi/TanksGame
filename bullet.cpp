#include "bullet.hpp"
#include <vector>
#include <utility>
using namespace std;


#define TIME_UNIT 20
#define NO_WALL make_pair(Point(-1, -1), Point(-1, -1))

Bullet::Bullet(int x, int y, int dirx, int diry, int shooter_tank)
{
	posx = x;
	posy = y;
	dx = dirx;
	dy = diry;
	life = TIME_UNIT * 10;
	shooter = shooter_tank;
}

int Bullet::getX()
{
	return posx;
}

int Bullet::getY()
{
	return posy;
}

bool collideWithWall(int x, int y, pair<Point, Point> wall)
{
	Point p1 = wall.first, p2 = wall.second;
	if (p1.x == p2.x)
	{
		if (abs(x - p1.x) <= 5)
			if (p1.y <= (y - 5) && p2.y >= (y + 5))
				return 1;
	}
	if (p1.y == p2.y)
	{
		if (abs(y - p1.y) <= 5)
			if (p1.x <= (x - 5) && p2.x >= (x + 5))
				return 1;
	}
	return 0;
}

pair<Point, Point> collideWithWalls(int x, int y, vector < pair<Point, Point> > walls)
{
	for (int i = 0 ; i < walls.size() ; i++)
	{
		pair<Point, Point> current_wall = walls[i];
		if (collideWithWall(x, y, current_wall))
			return current_wall;
	}
	return NO_WALL;
}

bool noCollision(pair<Point, Point> wall)
{
	return wall.first.x == -1 && wall.first.y == -1 && wall.second.x == -1 && wall.second.y == -1;
}

void Bullet::move(vector < pair<Point, Point> > walls)
{
	pair<Point, Point> collided_wall = collideWithWalls(posx, posy, walls);
	if (noCollision(collided_wall))
	{
		posx += dx;
		posy += dy;
	}
	else
	{
		Point p1 = collided_wall.first, p2 = collided_wall.second;
		
		if (p1.x == p2.x)
			dx *= -1;
		else
			dy *= -1;
		posx += dx;
		posy += dy;
	}

	life--;
}

int Bullet::getLife()
{
	return life;
}

int Bullet::getShooter()
{
	return shooter;
}